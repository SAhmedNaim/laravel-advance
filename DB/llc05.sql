-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2019 at 06:13 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `llc05`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mia Cassin III', 'mia-cassin-iii', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(2, 'Melvin Douglas', 'melvin-douglas', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(3, 'Lyda Hand', 'lyda-hand', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(4, 'Eulalia Kerluke', 'eulalia-kerluke', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(5, 'Verdie Reynolds', 'verdie-reynolds', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(6, 'Karlie Nienow', 'karlie-nienow', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(7, 'Charlene Armstrong IV', 'charlene-armstrong-iv', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(8, 'Idell Armstrong', 'idell-armstrong', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(9, 'Miss Erika Boehm', 'miss-erika-boehm', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(10, 'Dr. Douglas Pouros Jr.', 'dr-douglas-pouros-jr', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(11, 'Jarred Von DDS', 'jarred-von-dds', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(12, 'Caitlyn Robel', 'caitlyn-robel', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(13, 'Corrine Block', 'corrine-block', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(14, 'Ms. Arlene Morar PhD', 'ms-arlene-morar-phd', 1, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(15, 'Emmett Crooks Sr.', 'emmett-crooks-sr', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(16, 'Missouri Quitzon', 'missouri-quitzon', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(17, 'Prof. Mable Volkman PhD', 'prof-mable-volkman-phd', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(18, 'Ms. Octavia Ritchie', 'ms-octavia-ritchie', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(19, 'Prof. Amir Padberg II', 'prof-amir-padberg-ii', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(20, 'Retha Barrows', 'retha-barrows', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(21, 'Dr. Lesley Littel', 'dr-lesley-littel', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(22, 'Ambrose VonRueden', 'ambrose-vonrueden', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(23, 'Turner Robel', 'turner-robel', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(24, 'Dr. Ernesto Becker DVM', 'dr-ernesto-becker-dvm', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(25, 'Elias Kiehn', 'elias-kiehn', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(26, 'Letitia Stiedemann', 'letitia-stiedemann', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(27, 'Nathanael Bradtke III', 'nathanael-bradtke-iii', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(28, 'Dr. Reece Prohaska', 'dr-reece-prohaska', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(29, 'Royal Koch', 'royal-koch', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(30, 'Richard Schulist', 'richard-schulist', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(31, 'Vella Gerhold', 'vella-gerhold', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(32, 'Ara Yundt', 'ara-yundt', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(33, 'Miss Sadie Effertz', 'miss-sadie-effertz', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(34, 'Evan Leannon', 'evan-leannon', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(35, 'Robyn Braun', 'robyn-braun', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(36, 'Schuyler Labadie', 'schuyler-labadie', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(37, 'Miss Lila Fahey PhD', 'miss-lila-fahey-phd', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(38, 'Sid Feil', 'sid-feil', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(39, 'Leo Kutch', 'leo-kutch', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35'),
(40, 'Bethel Schimmel', 'bethel-schimmel', 1, '2019-04-26 05:55:35', '2019-04-26 05:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(29, '2014_10_12_000000_create_users_table', 1),
(30, '2014_10_12_100000_create_password_resets_table', 1),
(31, '2019_04_19_035851_create_categories_table', 1),
(32, '2019_04_19_040750_create_posts_table', 1),
(33, '2019_05_19_154205_create_notifications_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('dfa640ae-f132-4c5e-a276-e720b962c654', 'App\\Notifications\\NotifyAdmin', 'App\\Models\\User', 40, '{\"username\":\"alamgir hossain jadid\"}', '2019-05-19 10:04:34', '2019-05-19 09:55:27', '2019-05-19 10:04:34');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `category_id`, `title`, `content`, `thumbnail_path`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 31, 'Alice, \'or perhaps they won\'t.', 'Alice opened the door that led into the sky all the jurymen on to her head, she tried to say it any longer than that,\' said the Lory. Alice replied in an undertone to the three gardeners instantly.', 'https://lorempixel.com/640/480/?73129', 'draft', '2019-04-26 05:55:36', '2019-04-26 05:55:36'),
(2, 10, 21, 'YOU must cross-examine THIS.', 'English); \'now I\'m opening out like the look of the sort. Next came the guests, mostly Kings and Queens, and among them Alice recognised the White Rabbit with pink eyes ran close by her. There was.', 'https://lorempixel.com/640/480/?80994', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(3, 9, 25, 'Duck and a long tail.', 'Dormouse; \'--well in.\' This answer so confused poor Alice, \'to speak to this last word with such sudden violence that Alice quite jumped; but she got into a small passage, not much surprised at.', 'https://lorempixel.com/640/480/?77697', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(4, 2, 11, 'Hatter: \'but you could keep it.', 'I BEG your pardon!\' cried Alice again, in a whisper, half afraid that she began thinking over all the things being alive; for instance, there\'s the arch I\'ve got to?\' (Alice had no idea what a.', 'https://lorempixel.com/640/480/?76797', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(5, 9, 21, 'Tell her to begin.\' For, you.', 'Footman, and began to get very tired of being all alone here!\' As she said to herself how this same little sister of hers that you think I can do no more, whatever happens. What WILL become of it.', 'https://lorempixel.com/640/480/?36804', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(6, 4, 36, 'Alice said nothing: she had.', 'Dormouse go on in the air. \'--as far out to sea as you say it.\' \'That\'s nothing to do: once or twice, half hoping that the mouse doesn\'t get out.\" Only I don\'t take this young lady to see what the.', 'https://lorempixel.com/640/480/?15379', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(7, 9, 20, 'Cat again, sitting on a branch.', 'There was a different person then.\' \'Explain all that,\' he said in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up and went by without noticing her. Then followed the Knave of.', 'https://lorempixel.com/640/480/?87253', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(8, 4, 25, 'Here the Queen said to the.', 'Cat, as soon as look at all fairly,\' Alice began, in rather a hard word, I will prosecute YOU.--Come, I\'ll take no denial; We must have a prize herself, you know,\' the Hatter went on, taking first.', 'https://lorempixel.com/640/480/?87146', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(9, 9, 29, 'Alice. \'I\'m glad they\'ve begun.', 'However, I\'ve got to the waving of the right-hand bit to try the effect: the next thing was snorting like a mouse, That he met in the sea!\' cried the Gryphon. \'How the creatures argue. It\'s enough.', 'https://lorempixel.com/640/480/?33030', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(10, 2, 37, 'Alice. \'It must have a trial.', 'Alice, every now and then treading on my tail. See how eagerly the lobsters and the White Rabbit: it was getting quite crowded with the strange creatures of her head was so ordered about in the.', 'https://lorempixel.com/640/480/?79641', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(11, 8, 25, 'However, the Multiplication.', 'Alice guessed who it was, even before she got to go on in the world she was small enough to look through into the sea, \'and in that case I can listen all day about it!\' and he wasn\'t one?\' Alice.', 'https://lorempixel.com/640/480/?76184', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(12, 10, 12, 'VERY deeply with a round face.', 'March Hare said--\' \'I didn\'t!\' the March Hare will be much the most curious thing I ask! It\'s always six o\'clock now.\' A bright idea came into Alice\'s shoulder as she was losing her temper. \'Are you.', 'https://lorempixel.com/640/480/?52427', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(13, 5, 39, 'Indeed, she had been to her.', 'The other side of the song, \'I\'d have said to the Gryphon. \'--you advance twice--\' \'Each with a pair of gloves and a pair of boots every Christmas.\' And she began again: \'Ou est ma chatte?\' which.', 'https://lorempixel.com/640/480/?45704', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(14, 7, 21, 'She had just begun to think.', 'Hatter went on, \'that they\'d let Dinah stop in the other: he came trotting along in a long, low hall, which was immediately suppressed by the way, was the White Rabbit, \'and that\'s the queerest.', 'https://lorempixel.com/640/480/?92956', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(15, 7, 4, 'Dormouse sulkily remarked, \'If.', 'Alice! when she had found her way out. \'I shall sit here,\' the Footman went on muttering over the verses the White Rabbit returning, splendidly dressed, with a deep sigh, \'I was a sound of many.', 'https://lorempixel.com/640/480/?10870', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(16, 8, 15, 'Queen jumped up in such a.', 'Mock Turtle. \'And how do you want to stay with it as she swam about, trying to put the hookah out of sight before the trial\'s over!\' thought Alice. The poor little feet, I wonder if I like being.', 'https://lorempixel.com/640/480/?73562', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(17, 7, 32, 'Allow me to introduce some.', 'VERY tired of being all alone here!\' As she said to herself how this same little sister of hers would, in the wood, \'is to grow larger again, and all dripping wet, cross, and uncomfortable. The.', 'https://lorempixel.com/640/480/?25187', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(18, 7, 17, 'Alice rather unwillingly took.', 'Cat. \'I\'d nearly forgotten to ask.\' \'It turned into a pig,\' Alice quietly said, just as I\'d taken the highest tree in front of them, and considered a little, and then I\'ll tell you what year it is?\'.', 'https://lorempixel.com/640/480/?78568', 'draft', '2019-04-26 05:55:37', '2019-04-26 05:55:37'),
(19, 3, 26, 'So they couldn\'t see it?\' So.', 'Majesty!\' the Duchess was sitting on the other guinea-pig cheered, and was going to leave it behind?\' She said this last word with such a rule at processions; \'and besides, what would happen next.', 'https://lorempixel.com/640/480/?44636', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(20, 2, 12, 'Duchess said to the voice of.', 'It was the only one way of speaking to it,\' she thought, and rightly too, that very few little girls of her childhood: and how she was dozing off, and found that her flamingo was gone across to the.', 'https://lorempixel.com/640/480/?72091', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(21, 7, 18, 'Alice, and tried to open them.', 'Alas! it was certainly English. \'I don\'t think they play at all a proper way of speaking to it,\' she said to the whiting,\' said Alice, a good opportunity for making her escape; so she went back to.', 'https://lorempixel.com/640/480/?70533', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(22, 4, 2, 'Hatter. \'It isn\'t directed at.', 'Time, and round goes the clock in a very pretty dance,\' said Alice loudly. \'The idea of the birds and animals that had made the whole head appeared, and then sat upon it.) \'I\'m glad I\'ve seen that.', 'https://lorempixel.com/640/480/?43052', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(23, 4, 30, 'I wish I had our Dinah here, I.', 'I can\'t show it you myself,\' the Mock Turtle sighed deeply, and began, in a melancholy way, being quite unable to move. She soon got it out again, and Alice was a large plate came skimming out.', 'https://lorempixel.com/640/480/?49032', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(24, 9, 8, 'I could say if I like being.', 'King triumphantly, pointing to the Mock Turtle. Alice was more and more puzzled, but she had wept when she had found the fan and gloves, and, as the Dormouse said--\' the Hatter continued, \'in this.', 'https://lorempixel.com/640/480/?36335', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(25, 9, 16, 'For instance, if you like!\'.', 'However, \'jury-men\' would have this cat removed!\' The Queen smiled and passed on. \'Who ARE you doing out here? Run home this moment, I tell you!\' said Alice. \'I\'m glad they don\'t seem to have.', 'https://lorempixel.com/640/480/?97799', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(26, 7, 26, 'Ma!\' said the Dodo, \'the best.', 'Caterpillar\'s making such a thing as \"I sleep when I find a number of executions the Queen shouted at the cook, to see that she tipped over the fire, licking her paws and washing her face--and she.', 'https://lorempixel.com/640/480/?34048', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(27, 1, 9, 'YOUR temper!\' \'Hold your.', 'WHAT? The other guests had taken advantage of the trial.\' \'Stupid things!\' Alice thought over all she could guess, she was quite impossible to say to itself, \'Oh dear! Oh dear! I\'d nearly forgotten.', 'https://lorempixel.com/640/480/?74471', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(28, 3, 32, 'Alice again, for this time.', 'Alice like the look of things at all, as the Lory positively refused to tell him. \'A nice muddle their slates\'ll be in a very deep well. Either the well was very fond of pretending to be afraid of.', 'https://lorempixel.com/640/480/?73895', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(29, 4, 2, 'The Mouse looked at the.', 'Duchess was VERY ugly; and secondly, because she was walking hand in hand with Dinah, and saying to herself, rather sharply; \'I advise you to leave the room, when her eye fell on a summer day: The.', 'https://lorempixel.com/640/480/?49245', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(30, 9, 8, 'VERY deeply with a kind of.', 'The players all played at once in the window, she suddenly spread out her hand again, and said, \'That\'s right, Five! Always lay the blame on others!\' \'YOU\'D better not do that again!\' which produced.', 'https://lorempixel.com/640/480/?26683', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(31, 8, 11, 'Dinah my dear! Let this be a.', 'Alice, very earnestly. \'I\'ve had nothing else to say it any longer than that,\' said the Mouse in the beautiful garden, among the trees, a little now and then at the proposal. \'Then the eleventh day.', 'https://lorempixel.com/640/480/?89597', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(32, 9, 7, 'Dormouse again, so she went on.', 'Caterpillar. Alice said nothing; she had this fit) An obstacle that came between Him, and ourselves, and it. Don\'t let him know she liked them best, For this must ever be A secret, kept from all the.', 'https://lorempixel.com/640/480/?13278', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(33, 4, 2, 'Puss,\' she began, in a.', 'Indeed, she had asked it aloud; and in a low voice, \'Your Majesty must cross-examine THIS witness.\' \'Well, if I only knew how to speak again. In a minute or two, and the small ones choked and had to.', 'https://lorempixel.com/640/480/?57850', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(34, 5, 30, 'Duchess said to herself, \'I.', 'Alice and all of them were animals, and some \'unimportant.\' Alice could not remember ever having heard of one,\' said Alice, seriously, \'I\'ll have nothing more happened, she decided to remain where.', 'https://lorempixel.com/640/480/?46673', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(35, 5, 15, 'King. \'I can\'t remember things.', 'Mock Turtle said: \'advance twice, set to work shaking him and punching him in the same thing as \"I sleep when I find a thing,\' said the White Rabbit. She was looking down at once, in a low voice.', 'https://lorempixel.com/640/480/?70829', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(36, 8, 26, 'The soldiers were always.', 'Caterpillar took the place where it had entirely disappeared; so the King said to herself how this same little sister of hers would, in the last few minutes she heard the Queen said severely \'Who is.', 'https://lorempixel.com/640/480/?97785', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(37, 8, 7, 'CURTSEYING as you\'re falling.', 'Bill!\' then the Rabbit\'s voice along--\'Catch him, you by the White Rabbit, who said in a minute, trying to make herself useful, and looking at the March Hare and his buttons, and turns out his.', 'https://lorempixel.com/640/480/?18652', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(38, 1, 8, 'Then it got down off the fire.', 'I think--\' (she was rather doubtful whether she could get to the end: then stop.\' These were the cook, and a bright idea came into her face. \'Wake up, Alice dear!\' said her sister; \'Why, what a Mock.', 'https://lorempixel.com/640/480/?36115', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(39, 7, 13, 'I don\'t know,\' he went on.', 'Then it got down off the fire, and at last came a rumbling of little pebbles came rattling in at all?\' said Alice, rather doubtfully, as she had accidentally upset the milk-jug into his cup of tea.', 'https://lorempixel.com/640/480/?70073', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(40, 8, 13, 'And he added in a very good.', 'I must be Mabel after all, and I could say if I would talk on such a tiny little thing!\' said Alice, quite forgetting her promise. \'Treacle,\' said the Gryphon replied very solemnly. Alice was very.', 'https://lorempixel.com/640/480/?73597', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(41, 1, 1, 'I\'d only been the whiting,\'.', 'Alice very politely; but she ran out of the Lobster; I heard him declare, \"You have baked me too brown, I must sugar my hair.\" As a duck with its mouth and yawned once or twice, and shook itself.', 'https://lorempixel.com/640/480/?40508', 'draft', '2019-04-26 05:55:38', '2019-04-26 05:55:38'),
(42, 4, 19, 'Why, I do hope it\'ll make me.', 'King, \'that saves a world of trouble, you know, with oh, such long ringlets, and mine doesn\'t go in at the Mouse\'s tail; \'but why do you know that Cheshire cats always grinned; in fact, I didn\'t.', 'https://lorempixel.com/640/480/?39226', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(43, 4, 27, 'English!\' said the King.', 'VERY much out of the treat. When the Mouse had changed his mind, and was gone across to the Cheshire Cat, she was now only ten inches high, and she went round the thistle again; then the different.', 'https://lorempixel.com/640/480/?54355', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(44, 2, 36, 'ME.\' \'You!\' said the Hatter.', 'I\'m never sure what I\'m going to leave off this minute!\' She generally gave herself very good height indeed!\' said Alice, seriously, \'I\'ll have nothing more happened, she decided on going into the.', 'https://lorempixel.com/640/480/?12135', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(45, 6, 32, 'English, who wanted leaders.', 'Duchess: \'what a clear way you have just been reading about; and when Alice had begun to think that very few things indeed were really impossible. There seemed to be listening, so she began again.', 'https://lorempixel.com/640/480/?82848', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(46, 10, 34, 'NOT, being made entirely of.', 'Mary Ann, and be turned out of the sort. Next came an angry tone, \'Why, Mary Ann, what ARE you doing out here? Run home this moment, and fetch me a pair of white kid gloves: she took up the fan and.', 'https://lorempixel.com/640/480/?86901', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(47, 7, 2, 'Queen, and Alice rather.', 'The Duchess took no notice of her going, though she knew that were of the mushroom, and her face in some book, but I don\'t want to be?\' it asked. \'Oh, I\'m not looking for the next witness was the.', 'https://lorempixel.com/640/480/?33685', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(48, 5, 9, 'I should think very likely.', 'The twelve jurors were writing down \'stupid things!\' on their faces, so that it might tell her something worth hearing. For some minutes the whole party swam to the other, and making quite a new.', 'https://lorempixel.com/640/480/?82583', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(49, 6, 36, 'I mean what I was thinking I.', 'Duck. \'Found IT,\' the Mouse replied rather crossly: \'of course you know about this business?\' the King exclaimed, turning to the heads of the goldfish kept running in her French lesson-book. The.', 'https://lorempixel.com/640/480/?52879', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39'),
(50, 6, 35, 'YOU like cats if you were.', 'Cat again, sitting on a little bottle on it, and finding it very nice, (it had, in fact, a sort of present!\' thought Alice. \'I\'m glad I\'ve seen that done,\' thought Alice. \'I\'m glad they don\'t seem.', 'https://lorempixel.com/640/480/?78580', 'draft', '2019-04-26 05:55:39', '2019-04-26 05:55:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified` tinyint(4) NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verification_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `photo`, `email_verified`, `email_verified_at`, `email_verification_token`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'eliezer88@mcdermott.com', 'white.pasquale', '$2y$10$mE6wEpNsAQUBbKAsgyfNk.uoKdcRVochBwBfZ9UcjIwX0HJseq8h.', 'https://lorempixel.com/640/480/?45827', 1, '2019-04-26 05:55:33', '', '88H0If6wXq', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(2, 'jamil.dietrich@hotmail.com', 'zondricka', '$2y$10$.HfCQId/78S6CGeiq0RduenGKMeK4eVAOYSVy5M.Q0K6tX3tIvX8u', 'https://lorempixel.com/640/480/?25319', 1, '2019-04-26 05:55:33', '', 'y4Awhh0InZ', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(3, 'ydurgan@carter.org', 'bret11', '$2y$10$zCe7svhkPB/tw1j08e.1euBbM1AlolD1bz9xgVd9yhjlbWpRtfQGq', 'https://lorempixel.com/640/480/?28239', 1, '2019-04-26 05:55:33', '', '3fFWmPFUXS', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(4, 'gschuppe@dibbert.info', 'norwood.dach', '$2y$10$beoGmULIR3qLnR/xxx3h7.zfKA9QuvXzvKfiv6NWBYArTOPF.70fu', 'https://lorempixel.com/640/480/?59091', 1, '2019-04-26 05:55:33', '', 'UE6g2nu1ap', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(5, 'drake58@bernhard.com', 'erica.gaylord', '$2y$10$jmCryvxMc3ZJdL3kmu15/.HZMpuF6cC7SKjb8Nyp7EURJ5X4JuBxq', 'https://lorempixel.com/640/480/?33525', 1, '2019-04-26 05:55:33', '', 'f0qUzgYwIs', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(6, 'thalia.mckenzie@nader.com', 'percival.gaylord', '$2y$10$l4dN3jGCFri5//5f1/zJfuV7NTQ8s2ZJr3UG99K6iSpO1Zn6fySm.', 'https://lorempixel.com/640/480/?74471', 1, '2019-04-26 05:55:33', '', 'u7UHv3wJlk', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(7, 'thansen@gmail.com', 'kgleichner', '$2y$10$zhOxzqz7VnTJ71tjNhQySODA8Q0k7cSu80uQdaE7XjsuH.VgDKDGG', 'https://lorempixel.com/640/480/?54379', 1, '2019-04-26 05:55:33', '', 'SWXGTDYTtd', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(8, 'gaylord.lizeth@sporer.org', 'greilly', '$2y$10$XF8cAknebhTFcEAHAagtBusJoTpfMeAvpCTFubOYS6ZYtKDL4Yrai', 'https://lorempixel.com/640/480/?57922', 1, '2019-04-26 05:55:33', '', 'iXLHxFuwyn', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(9, 'ldickens@hotmail.com', 'bprice', '$2y$10$945zwW2AO7wr91sPhgGiQOIEdT.yP4zDRqD.7hd7/0rFyy7iwHOrS', 'https://lorempixel.com/640/480/?45039', 1, '2019-04-26 05:55:34', '', 'MWaZG71beV', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(10, 'alexandre.douglas@gmail.com', 'twest', '$2y$10$s3Nal0irIYQHfI7pPD2h7OoeTijO4UKFqkfSCZ1r/LaYfz9fZAgiq', 'https://lorempixel.com/640/480/?94471', 1, '2019-04-26 05:55:34', '', '06qfn3NHfn', NULL, '2019-04-26 05:55:34', '2019-04-26 05:55:34'),
(40, 'naim.ahmed035@gmail.com', 's ahmed naim', '$2y$10$OzIsomYVkfSMS6Wgz8ves.QtkdIuazYqkacCUm54MSvGR6f4tPLay', 'photo_5cc35284b47d23.53540197fdNdCP0toB.jpg', 1, '2019-05-19 09:56:28', '', NULL, NULL, '2019-04-26 12:48:36', '2019-05-19 09:56:28'),
(42, 'alamgir7373@gmail.com', 'alamgir hossain', '$2y$10$5MHz3qUw3D22AuErneVe8ejNetrKTLG1s6elr4XN4ZgE4y1Qo50y6', 'photo_5cc5d7397b5fd2.44671445euhKHyuwdl.jpg', 0, NULL, 'EwoIfoQcxWuFPp4mlU1YQXN0R9sDZlhK', NULL, NULL, '2019-04-28 10:39:21', '2019-04-28 10:39:21'),
(43, 'test@test.test', 'test user', '$2y$10$Oqsakk1XyOhz8YZyDnLrDOTBnrTDYhvENCKcbG3yJCGBuPW1hgZCi', 'photo_5cc93b4b1419a5.10126460jxmNv73pXE.jpg', 1, '2019-05-01 00:26:23', '', NULL, NULL, '2019-05-01 00:23:07', '2019-05-01 00:26:23'),
(44, 'naim.ahmed005@gmail.com', 'naim ahmed', '$2y$10$J5kM/OBAioThAxEd3Ku4.usgrGHVCk/SucYd6tFNzX27a/YLZur.S', 'photo_5ce16d9ab775b5.549122530ahuA2jpsj.png', 1, '2019-05-19 08:52:45', '', NULL, NULL, '2019-05-19 08:52:11', '2019-05-19 08:52:45'),
(46, 'alamgirhossain898@gmail.com', 'alamgir hossain jadid', '$2y$10$uQESBRoi0Z6ezDVgkRy9V.OVotQXhj81HJXFKQ43rRzXBtRjPUYFC', 'photo_5ce17c69035011.77650578R8r5ntJEZ8.jpg', 0, NULL, 'pqSQTfT5WapgbUHNQlBkDJ1Hk6EaBNyv', NULL, NULL, '2019-05-19 09:55:21', '2019-05-19 09:55:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
