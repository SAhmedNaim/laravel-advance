<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\FrontController@index')->name('/');
Route::get('post', 'Frontend\FrontController@post')->name('post');

// Route Group
Route::group(['prefix' => 'admin'], function() {
    Route::get('dashboard', 'Frontend\FrontController@index');
    Route::get('create', 'Frontend\FrontController@index');
    Route::get('list', 'Frontend\FrontController@index');
    Route::get('users', 'Frontend\FrontController@index');
});

// ==================================================================================
Route::get('register', 'Frontend\FrontController@register')->name('register');
Route::post('register', 'Frontend\FrontController@registerUser');

Route::get('verify/{token}', 'Frontend\FrontController@verifyEmail')->name('verify');

Route::get('login', 'Frontend\FrontController@login')->name('login');
Route::post('login', 'Frontend\FrontController@loginUser');

Route::get('logout', 'Frontend\FrontController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function() {

    Route::get('dashboard', 'Frontend\FrontController@dashboard')->name('dashboard');

    // categories
    Route::get('categories', 'Backend\CategoryController@index')->name('categories.index');
    Route::get('categories/add', 'Backend\CategoryController@create')->name('categories.create');
    Route::post('categories', 'Backend\CategoryController@store')->name('categories.store');
    Route::get('categories/{id}', 'Backend\CategoryController@show')->name('categories.show');
    Route::get('categories/{id}/edit', 'Backend\CategoryController@edit')->name('categories.edit');
    Route::put('categories/{id}', 'Backend\CategoryController@update')->name('categories.update');
    Route::delete('categories/{id}', 'Backend\CategoryController@delete')->name('categories.delete');

    // posts
    Route::resource('posts', 'Backend\PostController');

});
