<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'user_id'       => mt_rand(1,10),
        'category_id'   => mt_rand(1,40),
        'title'         => $faker->realText(32),
        'content'       => $faker->realText(),
        'thumbnail_path'=> $faker->imageUrl()
    ];
});
