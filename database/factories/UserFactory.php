<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'email'             => $faker->email,
        'username'          => $faker->userName,
        'password'          => bcrypt('123456'),
        'photo'             => $faker->imageUrl(),
        'remember_token'    => str_random(10),
        'email_verified'    => 1,
        'email_verified_at' => \Carbon\Carbon::now(),
        'email_verification_token'  => ''
    ];
});