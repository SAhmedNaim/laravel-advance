<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating single data
        // \App\Models\User::create([
        //     'email'     => 'naim.ahmed035@gmail.com',
        //     'username'  => 'SAhmedNaim',
        //     'password'  => bcrypt('123456'),
        //     'photo'     => 'photo_5cb95584014ea4.46130422kQ3r2v4xjX.jpg'
        // ]);

        // Creating multiple data using factory faker
        factory(\App\Models\User::class, 10)->create();
    }
}
