<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating single data
        // \App\Models\Category::insert([
        //     'name'  => 'World',
        //     'slug'  => str_slug('World'),

        // ], [
        //     'name'  => 'Design',
        //     'slug'  => str_slug('Design'),
        // ]);

        // Creating multiple data using factory faker
        factory(\App\Models\Category::class, 40)->create();
    }
}
