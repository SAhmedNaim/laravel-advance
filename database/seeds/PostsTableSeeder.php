<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating single data
        // \App\Models\Post::create([
        //     'user_id'       => 1,
        //     'category_id'   => 1,
        //     'title'         => 'Lorem Ipsum',
        //     'content'       => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry/=\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        //     'thumbnail_path'    => 'photo_5cb95584014ea4.46130422kQ3r2v4xjX.jpg'
        // ]);

        // Creating multiple data using factory faker
        factory(\App\Models\Post::class, 50)->create();
    }
}
