<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Post;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerificationEmail;
use Carbon\Carbon;
use Queue;
use App\Notifications\VerifyEmail;
use App\Notifications\NotifyAdmin;

class FrontController extends Controller
{
    public function index()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        // dd(cache()->get('articles')); // both are same
        // dd(cache('articles')); // both are same

        // Query using Laravel Cache
        $data['articles'] = cache('articles', function() {
            return Post::with('user', 'category')->orderBy('created_at', 'desc')->take(20)->get();
        });

        // Query using Database directly
        // $data['articles'] = Post::with('user', 'category')->orderBy('created_at', 'desc')->take(20)->get();

        return view('index', $data);
    }

    public function post()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        return view('post', $data);
    }

    public function register()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        return view('register', $data);
    }

    public function registerUser(Request $request)
    {
        /*
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required|min:6' 
        ]);
        */
        
        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'username'  => 'required|min:3',
            'password'  => 'required|min:6',
            'photo'     => 'required|image|max:10240'
        ], [
            'email'     => 'Email thik nai',
            'username'  => 'Valo naan den',
            'password'  => 'Password o thik nai',
            'photo'     => 'Photo den valo ekta'
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $photo = $request->file('photo');
        $file_name = uniqid('photo_', true).str_random(10) . '.' . $photo->getClientOriginalExtension();

        if($photo->isValid())
        {
            $photo->storeAs('images', $file_name);
        }
        // N.B: Here are some changes on 'config/filesystems.php'
        // change to => 
            // 'default' => env('FILESYSTEM_DRIVER', 'public'),
        // Then change to => 
            // 'public' => [
            //     'driver' => 'local',
            //     'root' => public_path('uploads'),
            //     'url' => env('APP_URL').'/storage',
            //     'visibility' => 'public',
            // ],


        // insert data to database
        // $user = new User;
        // $user->email    = trim($request->input('email'));
        // $user->username = trim($request->input('username'));
        // $user->password = bcrypt($request->input('password'));
        // $user->photo    = $file_name;
        // $user->save();

        $user = User::create([
            'email'     => strtolower(trim($request->input('email'))),
            'username'  => strtolower(trim($request->input('username'))),
            'password'  => bcrypt($request->input('password')),
            'photo'     => $file_name,
            'email_verification_token'  => str_random(32)
        ]);

        // send email
        // Mail::to($user->email)->send(new VerificationEmail($user)); // send with PHP script
        // Mail::to($user->email)->queue(new VerificationEmail($user)); // send with Redis server
        $user->notify(new VerifyEmail($user));

        // Database Notification to the Admin
        $admin = User::find(40);
        $admin->notify(new NotifyAdmin($user));

        session()->flash('message', 'Registration Successful');

        return redirect()->back();
    }

    public function verifyEmail($token = null)
    {        
        if($token == null)
        {
            session()->flash('type', 'warning');
            session()->flash('message', 'Invalid token');

            return redirect()->route('login');
        }

        $user = User::where('email_verification_token', $token)->first();

        if($user == null)
        {
            session()->flash('type', 'warning');
            session()->flash('message', 'Invalid token');

            return redirect()->route('login');
        }

        $user->update([
            'email_verified'            => 1,
            'email_verified_at'         => Carbon::now(),
            'email_verification_token'  => ''
        ]);
        
        session()->flash('type', 'success');
        session()->flash('message', 'Your account has been activated. You may log in now.');

        return redirect()->route('login');
    }

    public function login()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        return view('login', $data);
    }

    public function loginUser(Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $credentials = $request->except('_token');
        
        if(auth()->attempt($credentials))
        {
            $user = auth()->user();

            if($user->email_verified == 0)
            {
                session()->flash('type', 'danger');
                session()->flash('message', 'Your account is now verified yet. Please verify your email.');

                auth()->logout();

                return redirect()->route('login');
            }

            return redirect()->route('dashboard');
        }

        // Visit: https://stackoverflow.com/questions/35821477/class-app-user-not-found-in-laravel-when-changing-the-namespace

        session()->flash('type', 'danger');
        session()->flash('message', 'Credentials incorrect');

        return redirect()->back()->withErrors($validator)->withInput();
    }

    public function logout()
    {
        auth()->logout();

        session()->flash('type', 'success');
        session()->flash('message', 'You have been logged out');

        return redirect()->route('login');
    }

    public function dashboard()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];
        $data['user'] = auth()->user();

        return view('dashboard', $data);
    }

}
