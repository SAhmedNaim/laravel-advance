<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        $data['categories'] = Category::select('id', 'name', 'slug', 'status')->paginate(10);
        // N.B: paginate() or simplePaginate()

        return view('backend.category.index', $data);
    }

    public function create()
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];

        return view('backend.category.create', $data);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|unique:categories,name',
            'status'    => 'required'
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        Category::create([
            'name'  => trim($request->input('name')),
            'slug'  => str_slug(trim($request->input('name'))),
            'status'=> $request->input('status')
        ]);

        session()->flash('type', 'success');
        session()->flash('message', 'Category Added');

        return redirect()->back();
    }

    public function show($id)
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];
        $data['category'] = Category::with('posts', 'posts.user')->select('id', 'name', 'slug', 'status', 'created_at')->find($id);

        return view('backend.category.show', $data);
    }

    public function edit($id)
    {
        $data = [];
        $data['site_title'] = 'Laravel Advanced';
        $data['links'] = [
            'Facebook'  => 'https://facebook.com',
            'Twitter'   => 'https://twitter.com',
            'Google'    => 'https://google.com',
            'YouTube'   => 'https://youtube.com',
            'LinkedIn'  => 'https://linkedin.com'
        ];
        $data['category'] = Category::select('id', 'name', 'slug', 'status', 'created_at')->find($id);

        return view('backend.category.edit', $data);
    }

    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|unique:categories,name,' . $id,
            'status'    => 'required'
        ]);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $category = Category::find($id);

        $category->update([
            'name'  => trim($request->input('name')),
            'slug'  => str_slug(trim($request->input('name'))),
            'status'=> $request->input('status')
        ]);

        session()->flash('type', 'success');
        session()->flash('message', 'Category Updated');

        return redirect()->back();
    }

    public function delete($id)
    {
        $category = Category::find($id);

        $category->delete();

        session()->flash('type', 'success');
        session()->flash('message', 'Category Deleted');

        return redirect()->route('categories.index');
    }

}
