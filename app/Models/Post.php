<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Events\PostCreated;
use App\Events\PostUpdated;
use App\Events\PostDeleted;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'category_id', 
        'title', 
        'content', 
        'thumbnail_path', 
        'status' 
    ];

    protected $dates = [
        'created_at'
    ];

    protected $dispatchesEvents = [
        'created'   => PostCreated::class,
        'updated'   => PostUpdated::class,
        'deleted'   => PostDeleted::class
    ];

    /**
     * Defining relationship between Post & Category Model.
     * Actually one post must have under a category
     * So, that's why we use the method named belongsTo()
     * And the method name should be single 
     * Because, from post to category referes to one to one relationship
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
