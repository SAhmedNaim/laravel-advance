<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Post;

class PostCacheListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        /* View log file to debug.
         * For this reason i need to use the method named info()
         * Log file directory is: storage/logs/laravel-year-month-date.log
         * First Remove all contents and save
         * Then execute application and view log file
         * N.B: cache()->get('articles') and cache('articles') both are same
         **/
        // info('Event Fired'); 
        cache()->forget('articles');
        // info(cache('articles'));
        $posts = Post::with('user', 'category')->orderBy('created_at', 'desc')->take(20)->get();
        cache()->forever('articles', $posts);
        // info(cache()->get('articles'));
    }
}
