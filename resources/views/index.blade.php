@extends('app')

@section('jumbotron')
    <!-- @include('partials.jumbotron') -->
@endsection

@section('content')
    <h3 class="pb-4 mb-4 font-italic border-bottom">
        From the Blog
    </h3>

    @foreach($articles as $article)

    <div class="blog-post">
        <h2 class="blog-post-title">{{ $article->title }}</h2>
        <p class="blog-post-meta">{{ $article->created_at->format('F d, Y h:s A') }}
            or ({{ $article->created_at->diffForHumans() }}) by 
            <a href="#">{{ $article->username }}</a> on 
            <a href="#">{{ $article->category->name }}</a>
        </p>
    </div><!-- /.blog-post -->

    @endforeach

    {{-- $articles->links() --}}

    <nav class="blog-pagination">
        <a class="btn btn-outline-primary" href="#">Older</a>
        <a class="btn btn-outline-secondary disabled" href="#" tabindex="-1" aria-disabled="true">Newer</a>
    </nav>
@endsection