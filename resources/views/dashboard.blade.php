@extends('app')

@section('content')

    <h4>Logged in successfully as {{ optional($user)->email }}</h4>

    <p>
        <img src="{{ url('uploads/images/'. optional($user)->photo) }}" alt="Missing" width="250px"/>
    </p>


    @if($user->id == 40)

    


    <hr/>
    <h2>Unread Notifications</h2> 
    <div class="well">
        @foreach($user->unreadNotifications as $notification)
            <li>{{ $notification->type }}</li>
            <p>{{ $notification->data['username'] }} just registered</p>
            @php $notification->markAsRead(); @endphp
        @endforeach
    </div>
    <hr/>

    <h2>All Notification</h2> 
    <div class="well">
        @foreach($user->notifications as $notification)
            <li>{{ $notification->type }}</li>
            <p>{{ $notification->data['username'] }} just registered</p>
            @php $notification->markAsRead(); @endphp
        @endforeach
    </div>
    <hr/>

    @endif

    <a href="{{ route('categories.index') }}" class="btn btn-info btn-block btn-lg">Category</a>
    <hr>
    <a href="{{ route('posts.index') }}" class="btn btn-info btn-block btn-lg">Post</a>

@endsection