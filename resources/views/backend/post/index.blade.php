@extends('app')

@section('content')

    <div class="well">

        <h2>Post List</h2>

        <p>
            <a href="{{ route('posts.create') }}" class="btn btn-success">Add Post</a>
        </p>

        @if(session('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Post Title</th>
                    <th>Category</th>
                    <th>Author</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            @foreach($posts as $post)
            
            <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->category->name }}</td>
                <td>{{ $post->user->username }}</td>
                <td>{{ $post->status === 1 ? 'Active' : 'Inactive' }}</td>
                <td>
                    <a href="{{ route('posts.show', $post->id) }}" class="btn btn-info">
                        Details
                    </a>
                </td>
            </tr>

            @endforeach

        </table>

        {!! $posts->links() !!}
    </div>

@endsection