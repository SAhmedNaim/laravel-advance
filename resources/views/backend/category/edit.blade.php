@extends('app')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        {{ $error }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(session('message'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <ul>
                <li>
                    {{ session('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </li>
            </ul>
        </div>
    @endif

    <h2>Edit Category</h2>

    <hr>

    <form action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
        
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Category Name</label>
            <input type="text" name="name" value="{{ $category->name }}" {{ old('name') }} class="form-control" id="name"  placeholder="Enter category name">
        </div>

        <div class="form-group">
            <label for="status">Status</label>
            <select name="status" class="form-control">
                <option value="1" @if($category->status === 1) selected @endif>Active</option>
                <option value="0" @if($category->status === 0) selected @endif>Inactive</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary btn-block">Edit Category</button>
    </form>

    <hr/>

    <p>
        <a href="{{ route('categories.index') }}" class="btn btn-primary btn-block">
            Back to Category List
        </a>
    </p>

@endsection